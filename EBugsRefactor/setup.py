# -*- coding: utf-8 -*-
from setuptools import setup, find_packages
import setuptools.command.build_py
import subprocess


class GenDocsCommand(setuptools.command.build_py.build_py):

    """Command to generate docs."""

    def run(self):
        subprocess.Popen(
            ['pdoc', '--html', 'ebugs_refactor/', '--html-dir=docs', '--overwrite'])
        setuptools.command.build_py.build_py.run(self)

try:
    long_description = open("README.md").read()
except IOError:
    long_description = ""

setup(
    name="ebugs_refactor",
    version="0.1.1",
    description="A package for locating and fixing Android energy greedy patterns, and also measure their energy influence.",
    license="MIT",
    author="Marco Couto",
    author_email="marcocouto90@gmail.com",
    packages=find_packages(),
    install_requires=[
        'pdoc',
        'lazyme',
        'pandas',
        'numpy'
    ],
    package_dir={'': '.'},
    long_description=long_description,
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    cmdclass={
        'gendocs': GenDocsCommand
    },
)
