package org.greenlab.coevolgy.util;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

/**
 * Created by marco on 24-04-2018.
 */

public class Tracer {

    private static String TAG = "[Co-Evolgy]";
    private static File sdCard = Environment.getExternalStorageDirectory();


    public static void trace(String eBugName) {
        File directory = new File(sdCard.getAbsolutePath() + "/trepn");
        int seed = getSeed();

        if (!directory.exists()) {
            directory.mkdirs();
        }

        File file = new File(sdCard.getAbsolutePath() + "/trepn/"+ "traces" + seed);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file, true);
        } catch (FileNotFoundException e) {
            Log.i(TAG, " Error opening traces file.");
        }

        OutputStreamWriter osw = new OutputStreamWriter(fOut);

        try {
            osw.write(eBugName + "\n");
            osw.flush();
            osw.close();
            Log.i(TAG, "traced " + eBugName);

        } catch (IOException e) {
            Log.i(TAG, " An error occurred while appending to traces file ");
            Log.e(TAG, e.getMessage());
        }

    }


    public static void traceMethod(String methodName) {
        File directory = new File(sdCard.getAbsolutePath() + "/trepn");
        int seed = getSeed();

        if (!directory.exists()) {
            directory.mkdirs();
        }

        File file = new File(sdCard.getAbsolutePath() + "/trepn/"+ "methodTraces" + seed);
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(file, true);
        } catch (FileNotFoundException e) {
            Log.e(TAG, " Error opening traces file.");
        }

        OutputStreamWriter osw = new OutputStreamWriter(fOut);

        try {
            osw.write(methodName + "\n");
            osw.flush();
            osw.close();
            //Log.i(TAG, "["+seed+"] traced " + methodName);

        } catch (IOException e) {
            Log.e(TAG, " An error occurred while appending to traces file ");
            Log.e(TAG, e.getMessage());
        }

    }

    private static int getSeed(){
        Scanner sc = null;
        File sdCard = Environment.getExternalStorageDirectory();
        File seed = new File(sdCard.getAbsolutePath() + "/trepn/MonkeySeed");
        try {
            sc = new Scanner(seed);
        } catch (FileNotFoundException e) {
           return 0;
        }
        return sc.nextInt();

    }

}