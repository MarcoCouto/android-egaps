<!DOCTYPE html>
<html lang="{{ site.locale }}">
  
  {% include d3-head.md %}

  <body>
  	
    {% include header.md %}

	<section class="main-content container-fluid" style="max-width: 100% !important">

      {{ content }}
      
      {% include footer.md %}

	</section>

  </body>
</html>