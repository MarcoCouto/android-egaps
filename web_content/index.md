---
layout: default
title: {{site.name}}
---


<div class="row">
	<div class="col-sm-3">
		<div class="card" style="height: 100%">
			<div class="card-header">
				<strong>Experiment Results</strong>
			</div>
			<div class="card-body">
				<h5 class="card-title">Links to Results Files</h5>
				<ol>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/data/raw_results.tar.gz">Raw data (JSON)</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/data/gains_results.tar.gz">Full data w/ gains (CSV)</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/pages/pattern_results">Per pattern data</a>
					</li>
				</ol>
			</div>
			<div class="card-footer text-muted text-right small">
				October 11, 2019
			</div>
		</div>
	</div>
	<div class="col-sm-5">
		<div class="card" style="height: 100%">
			<div class="card-header">
				<strong>Results Analysis</strong>
			</div>
			<div class="card-body">
				<h5 class="card-title">Additional Data Display</h5>
				<ol>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/pages/plots">EGAPs vs Combinations comparison (Sankey Diagram)</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/pages/overview_results">Gains results (Bar graphs & Boxplots)</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/pages/ranksums_results">Mann–Whitney U tests</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/pages/energy_tags"><b>RQ4</b> Guidelines: Combinations</a>
					</li>
				</ol>
			</div>
			<div class="card-footer text-muted text-right small">
				October 12, 2019
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="card" style="height: 100%">
			<div class="card-header">
				<strong>Repository</strong>
			</div>
			<div class="card-body">
				<h5 class="card-title">Android Repository & Detection Results</h5>
				<ol>
					<li>
						<a href="https://www.dropbox.com/s/bdbujueslnza6o2/repo_android.zip?dl=0">Source code</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/data/output_detector.tar.gz">EGAPs per app (JSON)</a>
					</li>
					<li>
						<a href="{{site.baseurl | prepend:site.url}}/pages/egap_distribution">EGAP distribution table</a>
					</li>
				</ol>
			</div>
			<div class="card-footer text-muted text-right small">
				October 11, 2019
			</div>
		</div>
	</div>
</div>