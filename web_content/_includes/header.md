
<ul class="menu">
	<li class="menu-item"><a href="{{site.baseurl | prepend:site.url}}">Home</a>
	</li>
	<li class="menu-item"><a href="{{site.baseurl | prepend:site.url}}/pages/pattern_results">Pattern Results</a>
	</li>
	<li class="menu-item"><a href="{{site.baseurl | prepend:site.url}}/pages/ranksums_results">Test Results</a>
	</li>
	<li class="menu-item"><a href="{{site.baseurl | prepend:site.url}}/pages/plots">Plots</a>
	</li>
</ul>

<section class="page-header">
	<h1 class="project-name">{{ site.title }}</h1>
	<h2 class="project-tagline">{{ site.tagline }}</h2>
</section>
