<head>
    <meta charset="UTF-8">
    <title>{{ site.title }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#157878">
    <script src="{{site.baseurl | prepend:site.url}}/js/d3.v3.min.js"></script>
    <script src="{{site.baseurl | prepend:site.url}}/js/sankey.js"></script>
    <script src="{{site.baseurl | prepend:site.url}}/js/d3.chart.min.js"></script>
    <script src="{{site.baseurl | prepend:site.url}}/js/d3.chart.sankey.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{site.baseurl | prepend:site.url}}/css/normalize.css">
    <link rel="stylesheet" href="{{site.baseurl | prepend:site.url}}/css/custom.css">
    <link rel="stylesheet" href="{{site.baseurl | prepend:site.url}}/css/cayman.css">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-149777389-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-149777389-1');
    </script>
    <style>
      #chart {
        height: 500px;
        font: 13px sans-serif;
      }
      .node rect {
        fill-opacity: .9;
        shape-rendering: crispEdges;
        stroke-width: 0;
      }
      .node text {
        font-size: 14px;
        font-weight: bold;
      }
      .sub-node text {
        display: none;
      }
      .link {
        fill: none;
        stroke: #5d5d5d;
        stroke-opacity: 0.7;
      }
      .legend {
        font-family: 'Open Sans', sans-serif;
        line-height: 0.4;
      }
      .bad-small { 
        color: #f0cf00;
      }
      .bad-medium { 
        color: #ff9e00;
      }
      .bad-large { 
        color: #d30000;
      }
      .bad-vlarge { 
        color: #9e0000;
      }
      .bad-huge { 
        color: #000000;
      }
      .good-small { 
        color: #ade0a6; 
      }
      .good-medium { 
        color: #00ff2a; 
      }
      .good-large { 
        color: #59b550; 
      }
      .good-vlarge { 
        color: #008b49; 
      }
      .good-huge { 
        color: #20571a; 
      }
      .show {
        display: block;
      }
    </style>
</head>
