---
layout: page
title: {{ site.name }}
---

# All _Mann–Whitney U_ tests performed

> This page contains the results of the _Mann–Whitney U_ tests performed in our study.
>
> In the first section, we included the table which refers to the results for the test between individual EGAPs.
>
> The second section contains the _Mann–Whitney U_ test results for every comparison between an EGAP <br/>
> and a combination where it occurs. It also contains the calculated _Cohen's d_ value for when statistical <br/>
> evidence was found in a test.


| Patterns |
| :------------- | :-------     | :------              | :------------------
| **#1** DrawAllocation | **#2** Wakelock      | **#3** Recycle              | **#4** ObsoleteLayoutParam
| **#5** ViewHolder     | **#6** HashMapUsage  | **#7** ExcessiveMethodCalls | **#8** MemberIgnoringMethod


### EGAPs vs EGAPs

|         | #1           |          |#2          |          | #3         |          | #4         |          | #5         |          | #6         |          | #7         |          | #8       |          |
| :------ | :------      | :------  | :------    | :------  | :------    | :------  | :------    | :------  | :------    | :------  | :------    | :------  | :------    | :------  | :------  | :------  |
|         | p value      | _stats_  | p value    | _stats_    | p value    | _stats_    | p value    | _stats_    | p value    | _stats_    | p value      | _stats_  | p value      | _stats_    | p value      | _stats_  |
| **#1**     | --           | _--_     | <span class="c-effective">**6.31E-13**</span>| _-7.194_ | <span class="c-effective">**1.75E-08**</span>| _-5.635_ | 9.80E-01   | _0.026_    | <span class="c-effective">**6.77E-03**</span>| _-2.708_ | 4.05E-01     | _-0.833_ | 7.47E-01     | _-0.322_   | 6.86E-01     | _-0.405_ |
| **#2**     | <span class="c-effective">**6.31E-13**</span>| _7.194_  | --           | _--_     | <span class="c-effective">**4.04E-08**</span>| _5.489_  | <span class="c-effective">**8.79E-14**</span>| _7.458_  | <span class="c-effective">**1.82E-10**</span>| _6.379_  | <span class="c-effective">**1.88E-14**</span>| _7.659_  | <span class="c-effective">**9.65E-13**</span>| _7.135_    | <span class="c-effective">**1.35E-13**</span>| _7.401_  |
| **#3**     | <span class="c-effective">**1.75E-08**</span>| _5.635_  | <span class="c-effective">**4.04E-08**</span>| _-5.489_ | --           | _--_     | <span class="c-effective">**6.46E-13**</span>| _7.190_  | <span class="c-effective">**4.87E-03**</span>| _2.815_  | <span class="c-effective">**7.00E-12**</span>| _6.858_  | <span class="c-effective">**2.15E-11**</span>| _6.696_    | <span class="c-effective">**4.81E-12**</span>| _6.911_  |
| **#4**     | 9.80E-01     | _-0.026_ | <span class="c-effective">**8.79E-14**</span>| _-7.458_ | <span class="c-effective">**6.46E-13**</span>| _-7.190_ | --           | _--_     | <span class="c-effective">**8.96E-04**</span>| _-3.321_ | <span class="c-effective">**4.87E-02**</span>| _-1.971_ | 3.87E-01     | _-0.864_   | 3.35E-01     | _-0.964_ |
| **#5**     | <span class="c-effective">**6.77E-03**</span>| _2.708_  | <span class="c-effective">**1.82E-10**</span>| _-6.379_ | <span class="c-effective">**4.87E-03**</span>| _-2.815_ | <span class="c-effective">**8.96E-04**</span>| _3.321_  | --           | _--_     | <span class="c-effective">**5.88E-03**</span>| _2.755_  | <span class="c-effective">**3.56E-03**</span>| _2.915_    | <span class="c-effective">**3.05E-03**</span>| _2.963_  |
| **#6**     | 4.05E-01     | _0.833_  | <span class="c-effective">**1.88E-14**</span>| _-7.659_ | <span class="c-effective">**7.00E-12**</span>| _-6.858_ | <span class="c-effective">**4.87E-02**</span>| _1.971_  | <span class="c-effective">**5.88E-03**</span>| _-2.755_ | --           | _--_     | 2.75E-01     | _1.091_    | 2.29E-01     | _1.203_  |
| **#7**     | 7.47E-01     | _0.322_  | <span class="c-effective">**9.65E-13**</span>| _-7.135_ | <span class="c-effective">**2.15E-11**</span>| _-6.696_ | 3.87E-01     | _0.864_  | <span class="c-effective">**3.56E-03**</span>| _-2.915_ | 2.75E-01     | _-1.091_ | --           | _--_       | 9.77E-01     | _0.029_  |
| **#8**     | 6.86E-01     | _0.405_  | <span class="c-effective">**1.35E-13**</span>| _-7.401_ | <span class="c-effective">**4.81E-12**</span>| _-6.911_ | 3.35E-01     | _0.964_  | <span class="c-effective">**3.05E-03**</span>| _-2.963_ | 2.29E-01     | _-1.203_ | 9.77E-01     | _-0.029_   | --           | _--_     |

<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

### EGAPs vs Combinations

#### DrawAllocation

|                         | Mann–Whitney U |           | Cohen's d |
|-------------------------|----------------|-----------|-----------|
|                         | p value        | _stats_   |           |
| #1 **vs** #1 + #6           | 5.27E-02       | _-1.9372_ | --        |
| #1 **vs** #1 + #7           | 8.52E-01       | _-0.1861_ | --        |
| #1 **vs** #1 + #8           | 1.60E-01       | _-1.4067_ | --        |
| #1 **vs** #1 + #6 + #7      | <span class="c-effective">**8.76E-04**</span>  | _-3.3276_ | 1.3765    |
| #1 **vs** #1 + #6 + #8      | <span class="c-effective">**5.33E-04**</span>  | _-3.4634_ | 1.0798    |
| #1 **vs** #1 + #7 + #8      | 7.41E-01       | _-0.3309_ | --        |
| #1 **vs** #1 + #6 + #7 + #8 | <span class="c-effective">**2.38E-02**</span>  | _-2.2604_ | 0.8737    |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

#### Recycle

|                    | Mann–Whitney U |          | Cohen's d |
|--------------------|----------------|----------|-----------|
|                    | p value        | _stats_  |           |
| #3 **vs** #3 + #6      | <span class="c-effective">**4.76E-08**</span>  | _5.4599_ | 1.3511    |
| #3 **vs** #3 + #4      | <span class="c-effective">**1.17E-06**</span>  | _4.8601_ | 1.2503    |
| #3 **vs** #3 + #8      | 7.49E-01       | _0.3201_ | --        |
| #3 **vs** #3 + #4 + #6 | 8.25E-01       | _0.2206_ | --        |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

#### ObsoleteLayoutParam

|                              | Mann–Whitney U |           | Cohen's d |
|------------------------------|----------------|-----------|-----------|
|                              | p value        | _stats_   |           |
| #4 **vs** #4 + #6                | 2.13E-01       | _1.2458_  | --        |
| #4 **vs** #3 + #4                | 1.93E-01       | _-1.3029_ | --        |
| #4 **vs** #4 + #5                | 9.07E-01       | _0.1172_  | --        |
| #4 **vs** #4 + #7                | <span class="c-effective">**3.81E-06**</span>  | _-4.6213_ | 0.3510    |
| #4 **vs** #4 + #8                | 7.19E-02       | _1.7999_  | --        |
| #4 **vs** #3 + #4 + #6           | <span class="c-effective">**8.73E-06**</span>  | _-4.4465_ | 1.0079    |
| #4 **vs** #4 + #5 + #6           | 4.79E-01       | _0.7082_  | --        |
| #4 **vs** #4 + #6 + #7           | 5.08E-01       | _0.6621_  | --        |
| #4 **vs** #4 + #6 + #8           | 2.71E-01       | _-1.1004_ | --        |
| #4 **vs** #4 + #5 + #7           | 3.18E-01       | _-0.9992_ | --        |
| #4 **vs** #4 + #5 + #8           | 3.57E-01       | _-0.9216_ | --        |
| #4 **vs** #4 + #7 + #8           | 1.43E-01       | _-1.4650_ | --        |
| #4 **vs** #4 + #5 + #6 + #7      | <span class="c-effective">**3.09E-03**</span>  | _2.9589_  | 0.9427    |
| #4 **vs** #4 + #5 + #6 + #8      | <span class="c-effective">**4.89E-02**</span>  | _-1.9694_ | 0.7646    |
| #4 **vs** #4 + #6 + #7 + #8      | 8.26E-01       | _0.2199_  | --        |
| #4 **vs** #4 + #5 + #7 + #8      | <span class="c-effective">**5.29E-07**</span>  | _-5.0156_ | 1.7327    |
| #4 **vs** #4 + #5 + #6 + #7 + #8 | <span class="c-effective">**3.20E-02**</span>  | _-2.1440_ | 0.7024    |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

#### ViewHolder

|                              | Mann–Whitney U |           | Cohen's d |
|------------------------------|----------------|-----------|-----------|
|                              | p value        | _stats_   |           |
| #5 **vs** #5 + #6                | 2.76E-01       | _1.0892_  | --        |
| #5 **vs** #4 + #5                | <span class="c-effective">**4.13E-02**</span>  | _2.0406_  | 0.8554    |
| #5 **vs** #5 + #7                | <span class="c-effective">**9.13E-04**</span>  | _3.3159_  | 0.7353    |
| #5 **vs** #5 + #8                | <span class="c-effective">**1.34E-02**</span>  | _2.4739_  | 0.8372    |
| #5 **vs** #4 + #5 + #6           | 1.90E-01       | _1.3097_  | --        |
| #5 **vs** #5 + #6 + #7           | 6.77E-01       | _0.4172_  | --        |
| #5 **vs** #5 + #6 + #8           | 7.41E-01       | _-0.3309_ | --        |
| #5 **vs** #4 + #5 + #7           | 9.30E-01       | _-0.0873_ | --        |
| #5 **vs** #4 + #5 + #8           | 9.77E-01       | _-0.0291_ | --        |
| #5 **vs** #5 + #7 + #8           | 3.88E-01       | _0.8634_  | --        |
| #5 **vs** #4 + #5 + #6 + #7      | <span class="c-effective">**7.62E-04**</span>  | _3.3664_  | 1.1262    |
| #5 **vs** #4 + #5 + #6 + #8      | 4.43E-01       | _-0.7664_ | --        |
| #5 **vs** #4 + #5 + #7 + #8      | <span class="c-effective">**1.88E-04**</span>  | _-3.7350_ | 1.0371    |
| #5 **vs** #4 + #5 + #6 + #7 + #8 | 1.01E-01       | _-1.6395_ | --        |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

#### HashMapUsage

|                              | Mann–Whitney U |           | Cohen's d |
|------------------------------|----------------|-----------|-----------|
|                              | p value        | _stats_   |           |
| #6 **vs** #1 + #6                | 8.63E-01       | _-0.1723_ | --        |
| #6 **vs** #4 + #6                | 7.54E-01       | _0.3128_  | --        |
| #6 **vs** #3 + #6                | 3.85E-01       | _0.8686_  | --        |
| #6 **vs** #5 + #6                | <span class="c-effective">**1.16E-02**</span>  | _2.5231_  | 0.5095    |
| #6 **vs** #6 + #7                | 1.49E-01       | _1.4422_  | --        |
| #6 **vs** #6 + #8                | 8.93E-02       | _-1.6993_ | --        |
| #6 **vs** #1 + #6 + #7           | <span class="c-effective">**1.66E-02**</span>  | _-2.3963_ | 0.9310    |
| #6 **vs** #1 + #6 + #8           | 1.43E-01       | _-1.4649_ | --        |
| #6 **vs** #3 + #4 + #6           | <span class="c-effective">**3.46E-04**</span>  | _-3.5779_ | 0.6373    |
| #6 **vs** #4 + #5 + #6           | <span class="c-effective">**4.89E-02**</span>  | _1.9694_  | 0.7039    |
| #6 **vs** #4 + #6 + #7           | 3.75E-01       | _0.8877_  | --        |
| #6 **vs** #4 + #6 + #8           | <span class="c-effective">**6.41E-03**</span>  | _-2.7260_ | 0.4611    |
| #6 **vs** #5 + #6 + #7           | 2.73E-01       | _1.0963_  | --        |
| #6 **vs** #5 + #6 + #8           | 1.52E-01       | _1.4339_  | --        |
| #6 **vs** #6 + #7 + #8           | <span class="c-effective">**6.23E-05**</span>  | _-4.0038_ | 0.5170    |
| #6 **vs** #1 + #6 + #7 + #8      | 7.64E-01       | _-0.3007_ | --        |
| #6 **vs** #4 + #5 + #6 + #7      | <span class="c-effective">**3.44E-04**</span>  | _3.5798_  | 1.0859    |
| #6 **vs** #4 + #5 + #6 + #8      | 3.57E-01       | _-0.9216_ | --        |
| #6 **vs** #4 + #6 + #7 + #8      | 1.98E-01       | _1.2874_  | --        |
| #6 **vs** #4 + #5 + #6 + #7 + #8 | 2.99E-01       | _-1.0381_ | --        |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

#### ExcessiveMethodCalls

|                              | Mann–Whitney U |           | Cohen's d |
|------------------------------|----------------|-----------|-----------|
|                              | p value        | _stats_   |           |
| #7 **vs** #1 + #7                | <span class="c-effective">**1.34E-04**</span>  | _3.8192_  | 1.1011    |
| #7 **vs** #6 + #7                | 2.90E-01       | _1.0587_  | --        |
| #7 **vs** #4 + #7                | <span class="c-effective">**3.65E-06**</span>  | _-4.6306_ | 0.3579    |
| #7 **vs** #5 + #7                | 7.76E-02       | _1.7648_  | --        |
| #7 **vs** #7 + #8                | 5.29E-01       | _-0.6303_ | --        |
| #7 **vs** #1 + #6 + #7           | 5.60E-02       | _-1.9112_ | --        |
| #7 **vs** #1 + #7 + #8           | <span class="c-effective">**1.72E-04**</span>  | _3.7571_  | 1.1276    |
| #7 **vs** #4 + #6 + #7           | 3.72E-01       | _-0.8930_ | --        |
| #7 **vs** #5 + #6 + #7           | 5.54E-01       | _-0.5918_ | --        |
| #7 **vs** #6 + #7 + #8           | <span class="c-effective">**4.43E-06**</span>  | _-4.5903_ | 0.3751    |
| #7 **vs** #4 + #5 + #7           | 3.37E-01       | _-0.9604_ | --        |
| #7 **vs** #4 + #7 + #8           | 7.01E-02       | _-1.8113_ | --        |
| #7 **vs** #5 + #7 + #8           | 7.34E-01       | _0.3395_  | --        |
| #7 **vs** #1 + #6 + #7 + #8      | 8.54E-01       | _0.1843_  | --        |
| #7 **vs** #4 + #5 + #6 + #7      | <span class="c-effective">**3.29E-03**</span>  | _2.9395_  | 0.8751    |
| #7 **vs** #4 + #6 + #7 + #8      | 6.05E-02       | _-1.8775_ | --        |
| #7 **vs** #4 + #5 + #7 + #8      | <span class="c-effective">**2.10E-07**</span>  | _-5.1903_ | 2.1278    |
| #7 **vs** #4 + #5 + #6 + #7 + #8 | <span class="c-effective">**1.75E-02**</span>  | _-2.3768_ | 0.8542    |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---

#### MemberIgnoringMethod

|                              | Mann–Whitney U |           | Cohen's d |
|------------------------------|----------------|-----------|-----------|
|                              | p value        | _stats_   |           |
| #8 **vs** #1 + #8                | <span class="c-effective">**4.76E-03**</span>  | _2.8231_  | 0.7073    |
| #8 **vs** #6 + #8                | 1.61E-01       | _-1.4028_ | --        |
| #8 **vs** #4 + #8                | 5.63E-01       | _0.5779_  | --        |
| #8 **vs** #3 + #8                | <span class="c-effective">**1.20E-02**</span>  | _-2.5127_ | 0.6529    |
| #8 **vs** #5 + #8                | <span class="c-effective">**1.57E-02**</span>  | _2.4157_  | 0.6929    |
| #8 **vs** #7 + #8                | 1.86E-01       | _-1.3213_ | --        |
| #8 **vs** #1 + #6 + #8           | <span class="c-effective">**1.07E-02**</span>  | _-2.5515_ | 1.0462    |
| #8 **vs** #1 + #7 + #8           | <span class="c-effective">**8.07E-04**</span>  | _3.3504_  | 0.8754    |
| #8 **vs** #4 + #6 + #8           | 1.03E-01       | _-1.6308_ | --        |
| #8 **vs** #5 + #6 + #8           | 8.52E-01       | _-0.1861_ | --        |
| #8 **vs** #6 + #7 + #8           | <span class="c-effective">**3.49E-06**</span>  | _-4.6396_ | 0.5323    |
| #8 **vs** #4 + #5 + #8           | 4.79E-01       | _-0.7082_ | --        |
| #8 **vs** #4 + #7 + #8           | 8.96E-01       | _0.1303_  | --        |
| #8 **vs** #5 + #7 + #8           | 6.91E-01       | _0.3978_  | --        |
| #8 **vs** #1 + #6 + #7 + #8      | 2.11E-01       | _-1.2515_ | --        |
| #8 **vs** #4 + #5 + #6 + #8      | 1.38E-01       | _-1.4843_ | --        |
| #8 **vs** #4 + #6 + #7 + #8      | 8.76E-02       | _1.7083_  | --        |
| #8 **vs** #4 + #5 + #7 + #8      | <span class="c-effective">**3.07E-06**</span>  | _-4.6664_ | 1.3798    |
| #8 **vs** #4 + #5 + #6 + #7 + #8 | <span class="c-effective">**4.67E-02**</span>  | _-1.9888_ | 0.5346    |


<span class="c-effective">**bold value**</span> => statistical significance (i.e. _rejected null hypothesis_)

---