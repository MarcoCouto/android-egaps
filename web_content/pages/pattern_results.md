---
layout: page
title: {{site.name}}
---

# Results Indexed by Pattern/Combination
> This page contains links to the results, both untreated (**raw**, in `json` format) and summarized with the gains calculated (**w/ gains**, in `csv` format).
>
> The results are indexed by pattern.


### Individual Patterns

* #1 - DrawAllocation       : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation.csv)

* #2 - Wakelock             : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/Wakelock.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/Wakelock.csv)

* #3 - Recycle              : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/Recycle.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/Recycle.csv)

* #4 - ObsoleteLayoutParam  : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ObsoleteLayoutParam.csv)

* #5 - ViewHolder           : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ViewHolder.csv)

* #6 - HashMapUsage         : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage.csv)

* #7 - ExcessiveMethodCalls : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls.csv)

* #8 - MemberIgnoringMethod : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/MemberIgnoringMethod.csv)



### Combinations

* #1 + #6 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_HashMapUsage.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_HashMapUsage.csv)

* #1 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_ExcessiveMethodCalls.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_ExcessiveMethodCalls.csv)

* #1 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_MemberIgnoringMethod.csv)

* #3 + #4 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ObsoleteLayoutParam_Recycle.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ObsoleteLayoutParam_Recycle.csv)

* #3 + #6 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_Recycle.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_Recycle.csv)

* #3 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/MemberIgnoringMethod_Recycle.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/MemberIgnoringMethod_Recycle.csv)

* #4 + #5 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #6 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_ObsoleteLayoutParam.csv)

* #4 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_ObsoleteLayoutParam.csv)

* #4 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/MemberIgnoringMethod_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/MemberIgnoringMethod_ObsoleteLayoutParam.csv)

* #5 + #6 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_ViewHolder.csv)

* #5 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_ViewHolder.csv)

* #5 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/MemberIgnoringMethod_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/MemberIgnoringMethod_ViewHolder.csv)

* #6 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage.csv)

* #6 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_MemberIgnoringMethod.csv)

* #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_MemberIgnoringMethod.csv)

* #1 + #6 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_HashMapUsage_ExcessiveMethodCalls.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_HashMapUsage_ExcessiveMethodCalls.csv)

* #1 + #6 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_HashMapUsage_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_HashMapUsage_MemberIgnoringMethod.csv)

* #1 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_ExcessiveMethodCalls_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_ExcessiveMethodCalls_MemberIgnoringMethod.csv)

* #3 + #4 + #6 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_ObsoleteLayoutParam_Recycle.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_ObsoleteLayoutParam_Recycle.csv)

* #4 + #5 + #6 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #5 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #5 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #6 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage_ObsoleteLayoutParam.csv)

* #4 + #6 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam.csv)

* #4 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_MemberIgnoringMethod_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_MemberIgnoringMethod_ObsoleteLayoutParam.csv)

* #5 + #6 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage_ViewHolder.csv)

* #5 + #6 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_MemberIgnoringMethod_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_MemberIgnoringMethod_ViewHolder.csv)

* #5 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_MemberIgnoringMethod_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_MemberIgnoringMethod_ViewHolder.csv)

* #6 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage_MemberIgnoringMethod.csv)

* #1 + #6 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/DrawAllocation_ExcessiveMethodCallsHashMapUsage_MemberIgnoringMethod.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/DrawAllocation_ExcessiveMethodCallsHashMapUsage_MemberIgnoringMethod.csv)

* #4 + #5 + #6 + #7 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage_ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #5 + #6 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #5 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.csv)

* #4 + #6 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam.csv)

* #4 + #5 + #6 + #7 + #8 : 	[`raw`]({{site.baseurl | prepend:site.url}}/data/json/ExcessiveMethodCalls_HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.json) & [`w/ gains`]({{site.baseurl | prepend:site.url}}/data/csv/ExcessiveMethodCalls_HashMapUsage_MemberIgnoringMethod_ObsoleteLayoutParam_ViewHolder.csv)

