---
layout: d3-page
title: {{ site.name }}
---

# Sankey Diagram

<div id="chart"></div>

<script>
  var colors = {
        'good': '#008744',
        'bad': '#d11141',
        'neutral': '#58668b',
        'node': '#1e1f26',
        'small-bad': '#f0cf00',
        'medium-bad': '#ff9e00',
        'large-bad': '#d30000',
        'very large-bad': '#9e0000',
        'huge-bad': '#000000',
        'small-good': '#ade0a6',
        'medium-good': '#00ff2a',
        'large-good': '#59b550',
        'very large-good': '#008b49',
        'huge-good': '#20571a'
      };
  d3.json("{{site.baseurl | prepend:site.url}}/data/sankey_data.json", function(error, json) {
    var chart = d3.select("#chart").append("svg").chart("Sankey.Path");
    chart
      .name(label)
      .colorNodes(function(name, node) {
        return color("node", 1) || colors.fallback;
      })
      .colorLinks(function(link) {
        //return color(link.source, 4) || color(link.target, 1) || colors.fallback;
        return color(link.effect + "-" + link.result, 4) || color(link.target, 1) || colors.fallback;
      })
      .nodeWidth(15)
      .nodePadding(10)
      .spread(true)
      .iterations(0)
      .draw(json);
    function label(node) {
      return node.name.replace(/\s*\(.*?\)$/, '');
    }
    function color(node, depth) {
      /*console.log(node);*/
      //var id = node.id.replace(/(_score)?(_\d+)?$/, '');
      var id = node;
      if (colors[id]) {
        return colors[id];
      } else if (depth > 0 && node.targetLinks && node.targetLinks.length == 1) {
        return color(node.targetLinks[0].source, depth-1);
      } else {
        return null;
      }
    }
  });
</script>
<div class="legend">
    <h3>Legend:</h3>
    <p><i class="good-small">&#9632;</i> | <i class="bad-small">&#9632;</i> : small effect size</p>
    <p><i class="good-medium">&#9632;</i> | <i class="bad-medium">&#9632;</i> : medium effect size</p>
    <p><i class="good-large">&#9632;</i> | <i class="bad-large">&#9632;</i> : large effect size</p>
    <p><i class="good-vlarge">&#9632;</i> | <i class="bad-vlarge">&#9632;</i> : very large effect size</p>
    <p><i class="good-huge">&#9632;</i> | <i class="bad-huge">&#9632;</i> : huge effect size</p>
</div>

---

### NOTE:
> This is the Sankey Diagram which represents the results of the _Mann–Whitney U_ test, performed between the gains 
> of each EGAP and the gains of each combination where the EGAP occurs.
>
> - **Green connections**: the _Mann–Whitney U_ test revealed that the combination has consistently higher gain values.
> - **Red connections**: the _Mann–Whitney U_ test revealed that the combination has consistently lower gain values.
> 
> The width of a connection indicates the result of the _Cohen's d_ value, as does the color intensity. 
