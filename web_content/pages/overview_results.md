---
layout: page
title: {{ site.name }}
---

# Plots for gains results
> This page contains the bar graphs & boxplots regarding the gains of each EGAP and combination.
>


### Bar Graphs

<div class="row">
	<div class="col-6">
		<img src="{{site.baseurl | prepend:site.url}}/img/plots/bar_1st_half_narrow.png" alt="1st Half Bar charts">
	</div>
	<div class="col-6">
		<img src="{{site.baseurl | prepend:site.url}}/img/plots/bar_2nd_half_narrow.png" alt="2nd Half Bar charts">
	</div>
</div>

---

### Boxplots

<div class="row">
	<div class="col-6">
		<img src="{{site.baseurl | prepend:site.url}}/img/plots/box_1st_half_narrow.png" alt="1st Half Boxplots charts">
	</div>
	<div class="col-6">
		<img src="{{site.baseurl | prepend:site.url}}/img/plots/box_2nd_half_narrow.png" alt="2nd Half Boxplots charts">
	</div>
</div>
