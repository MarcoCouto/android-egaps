---
layout: page
title: {{ site.name }}
---

# Energy tags for _all_ combinations

> This page contains, for each studied combination, the designated energy tag,
> in concordance with the classifications introduced in the answer to **RQ4** (Section _VI.C_).
>
> The colored bars below each combinations represent the type of connection that it has between 
> each individual refactoring: a <code class="bg-positive">green</code> bar indicates a _positive effect_ 
> connection, whereas a <code class="bg-negative">red</code> bar appears when there is a _negative effect_. 
> Grey bars appear when the _Mann-Whitney U_ test resulted in no statistical evidence.

| Patterns |
| :------------- | :-------     | :------              | :------------------
| **#1** DrawAllocation | **#2** Wakelock      | **#3** Recycle              | **#4** ObsoleteLayoutParam
| **#5** ViewHolder     | **#6** HashMapUsage  | **#7** ExcessiveMethodCalls | **#8** MemberIgnoringMethod


### List of Tags/Classifications (per combination)

<h5>
	<del>[#1, #3]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#3</div>
</div>

---

<h5>
	[#1, #6] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	[#1, #7] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#1, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#2, #6]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	<del>[#2, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#2, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#3, #4] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#4</div>
</div>

---

<h5>
	[#3, #6] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	<del>[#3, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#3, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #5] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#5</div>
</div>

---

<h5>
	[#4, #6] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	[#4, #7] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#4, #8] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#5, #6] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	[#5, #7] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#5, #8] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#6, #7] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#6, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#7, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#1, #3, #6]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	<del>[#1, #3, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#1, #3, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#1, #6, #7] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-right border-left bg-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#1, #6, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-right border-left bg-unk-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#1, #7, #8] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-right border-left bg-negative" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#2, #6, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#2, #6, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#2, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#3, #4, #6] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-right border-left bg-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	<del>[#3, #4, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#3, #4, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#3, #6, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#3, #6, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#3, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-right border-left bg-unknown" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #5, #6] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-right border-left bg-unk-negative" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#6</div>
</div>

---

<h5>
	[#4, #5, #7] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-right border-left bg-unk-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#4, #5, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-right border-left bg-unk-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #6, #7] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-right border-left bg-unk-negative" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#4, #6, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-right border-left bg-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #7, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-right border-left bg-unk-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#5, #6, #7] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-right border-left bg-unk-negative" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#5, #6, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-right border-left bg-unk-negative" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#5, #7, #8] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-right border-left bg-unk-negative" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#6, #7, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-right border-left bg-positive" role="progressbar" style="width: 34%" aria-valuenow="34" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 33%" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#1, #3, #6, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#1, #3, #6, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#1, #3, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#1, #6, #7, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left border-right bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unk-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#2, #6, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#2</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#3, #4, #6, #7]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	<del>[#3, #4, #6, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#3, #4, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#3, #6, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #5, #6, #7] : &nbsp; <code class="c-harmful">harmful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left border-right bg-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
</div>

---

<h5>
	[#4, #5, #6, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left border-right bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #5, #7, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left border-right bg-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #6, #7, #8] : &nbsp; <code class="c-helpful">helpful</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unk-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unk-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unk-positive" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unk-negative" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#5, #6, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#1, #3, #6, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#1</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	<del>[#3, #4, #6, #7, #8]</del> : &nbsp; <code class="c-untested">not tested</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#3</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-unknown" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>

---

<h5>
	[#4, #5, #6, #7, #8] : &nbsp; <code class="c-effective">effective</code>
</h5>
<div class="progress">
  <div class="progress-bar border-right bg-positive" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#4</div>
  <div class="progress-bar border-left border-right bg-unk-positive" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#5</div>
  <div class="progress-bar border-left border-right bg-unk-positive" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#6</div>
  <div class="progress-bar border-left border-right bg-positive" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#7</div>
  <div class="progress-bar border-left bg-positive" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">#8</div>
</div>
