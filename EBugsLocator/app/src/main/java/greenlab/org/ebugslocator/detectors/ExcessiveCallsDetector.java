package greenlab.org.ebugslocator.detectors;

import com.android.annotations.NonNull;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Location;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.intellij.psi.ImplicitVariable;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiArrayAccessExpression;
import com.intellij.psi.PsiAssignmentExpression;
import com.intellij.psi.PsiBinaryExpression;
import com.intellij.psi.PsiBlockStatement;
import com.intellij.psi.PsiDeclarationStatement;
import com.intellij.psi.PsiDoWhileStatement;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiExpressionStatement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiForStatement;
import com.intellij.psi.PsiIfStatement;
import com.intellij.psi.PsiLocalVariable;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiParenthesizedExpression;
import com.intellij.psi.PsiPostfixExpression;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiStatement;
import com.intellij.psi.PsiWhileStatement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import greenlab.org.ebugslocator.utils.Reporter;
import greenlab.org.ebugslocator.utils.StringUtils;

/**
 * Created by marco on 02-03-2018.
 */

public class ExcessiveCallsDetector extends Detector implements Detector.JavaPsiScanner {
    private static final Class<? extends Detector> DETECTOR_CLASS = ExcessiveCallsDetector.class;
    private static final EnumSet<Scope> DETECTOR_SCOPE = Scope.JAVA_FILE_SCOPE;

    private static final Implementation IMPLEMENTATION = new Implementation(
            DETECTOR_CLASS,
            DETECTOR_SCOPE
    );

    private static final String ISSUE_ID = "ExcessiveMethodCalls";
    private static final String ISSUE_DESCRIPTION = "Number of method calls can be reduced";
    private static final String ISSUE_EXPLANATION =
            "Calling methods inside loops, either in the loop condition or in the body, is " +
            "usually a good optimization target." // TODO: improve explanation
            ;
    private static final Category ISSUE_CATEGORY = Category.PERFORMANCE;
    private static final int ISSUE_PRIORITY = 5;
    private static final Severity ISSUE_SEVERITY = Severity.WARNING;

    public static final Issue ISSUE = Issue.create(
            ISSUE_ID,
            ISSUE_DESCRIPTION,
            ISSUE_EXPLANATION,
            ISSUE_CATEGORY,
            ISSUE_PRIORITY,
            ISSUE_SEVERITY,
            IMPLEMENTATION
    );

    private Map<String, List<String>> methodCalls;
    private Map<String, Set<String>> conditionedVars;
    private Set<String> classVars;
    private String visitingMethod;

    public ExcessiveCallsDetector() {
        this.methodCalls = new HashMap<>();
        this.conditionedVars = new HashMap<>();
        this.classVars = new HashSet<>();
        this.visitingMethod = "";
    }

    private String getVarFromExpression(PsiMethodCallExpression expression) {
        String qualifiedName = expression.getMethodExpression()
                                         .getQualifiedName()
                                         .replace("this","");

        if (qualifiedName.contains(".")) {
            String varName = qualifiedName.split("\\.")[0];
            if (classVars.contains(varName)) {
                return "this";
            }else{
                return varName;
            }
        }else{
            return "this";
        }
    }

    private String getVarFromExpression(PsiReferenceExpression expression) {
        String qualifiedName = expression.getQualifiedName();

        if (qualifiedName.startsWith("this.") || classVars.contains(qualifiedName)) return "this";
        if (qualifiedName.contains(".")) {
            return qualifiedName.split("\\.")[0];
        }else{
            return qualifiedName;
        }
    }

    private String getVarFromExpression(PsiExpression expression) {
        return ""; // FIXME: maybe try to detect the var on a generic expression (how??)
    }

    private String expressionTag(JavaContext context, PsiMethodCallExpression expression) {
        Location l = context.getLocation(expression);
        String start = "("+l.getStart().getLine()+","+l.getStart().getColumn()+")";
        String end = "("+l.getEnd().getLine()+","+l.getEnd().getColumn()+")";
        return "["+start+"-"+end+"]" + expression.getText();
    }

    private void debug() {
        System.out.println("{ VARS }");
        for (String method : this.conditionedVars.keySet()) {
            Set<String> set = this.conditionedVars.get(method);
            System.out.println("\t [ " + method + " ]");
            for (String var : set) {
                System.out.println("\t\t>> " + var);
            }
        }

        System.out.println("{ CALLS }");
        for (String method : this.methodCalls.keySet()) {
            List<String> list = this.methodCalls.get(method);
            System.out.println("\t [ " + method + " ]");
            for (String exp : list) {
                System.out.println("\t\t>> " + exp);
            }
        }
        System.out.println("\n\n");
    }

    /**
     * Selects the AST nodes for the detector to reach.
     * @return
     */
    /* FIXME: Variables with names already used as filed variables.
            When there's a class field named "X", and in a method a new variable
            is declared with the same name "X", they both are considered as fields */
    @Override
    public List<Class<? extends PsiElement>> getApplicablePsiTypes() {
        return Arrays.<Class<? extends PsiElement>>asList(PsiForStatement.class,
                                PsiWhileStatement.class,
                                PsiDoWhileStatement.class,
                                PsiMethod.class,
                                PsiMethodCallExpression.class,
                                PsiField.class);
    }

    @Override
    public void afterCheckProject(Context context) {
        if (context.getDriver().getPhase() < 3) {
            context.getDriver().requestRepeat(this, DETECTOR_SCOPE);
        }else{
            //debug();
        }
        super.afterCheckProject(context);
    }

    /**
     * Creates a Parse Tree visitor to analyze the AST of a file.
     * @param context
     * @return
     */
    @Override
    public JavaElementVisitor createPsiVisitor(@NonNull JavaContext context) {
        int phase = context.getDriver().getPhase();
        if (phase == 1){
            // First AST traversal.
            // Look for class variables (really quick).
            return new ClassVarsChecker(context, phase);

        }else if (phase == 2){
            // Second AST traversal.
            // Look for variables that are updated inside a loop, and
            // also gather info about ALL existing method calls.
            return new LoopChecker(context, phase);
        }else {
            // Third (final) AST traversal.
            // Check if the previously detected method calls can be
            // passed outside the loop
            return new MethodCallChecker(context, phase);
        }
    }


    private class LoopChecker extends JavaElementVisitor {
        /*
            How to determine if a method call can be passed outside the loop?
            RULES: (1) When the left operand of an assignment is a method call AND the variable is
                       not in the loop condition.
                   (2) When variables involved in a 1-operand statement are not used in the loop
                       condition.

            (+) For both rules, when a method invoked inside the loop has no relation to  variables
                updated inside the loop, it can be passed outside.
            */

        private final JavaContext mContext;
        private int phase;

        private boolean insideLoop;
        private boolean insideAssignmentLO;
        private boolean insideAssignmentRO;
        private boolean insideLoopCondition;
        private boolean insideLoopUpdate;

        public LoopChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
            this.insideLoop = false;
            this.insideAssignmentLO = false;
            this.insideAssignmentRO = false;
            this.insideLoopCondition = false;
            this.insideLoopUpdate = false;
        }

        private void addConditionedVar(String varName) {
            if (conditionedVars.containsKey(visitingMethod)) {
                conditionedVars.get(visitingMethod).add(varName);
            }else{
                Set<String> set = new HashSet<>();
                set.add(varName);
                conditionedVars.put(visitingMethod, set);
            }
        }

        private void addMethodCall(PsiMethodCallExpression expression) {
            if (methodCalls.containsKey(visitingMethod)) {
                methodCalls.get(visitingMethod).add(expressionTag(mContext, expression));
            }else{
                ArrayList<String> list = new ArrayList<>();
                list.add(expressionTag(mContext, expression));
                methodCalls.put(visitingMethod, list);
            }
        }

        @Override
        public void visitForStatement(PsiForStatement statement) {
            this.insideLoop = true;

            this.insideLoopCondition = true;
            if (statement.getCondition() != null) statement.getCondition().accept(this);
            this.insideLoopCondition = false;

            this.insideLoopUpdate = true;
            if (statement.getUpdate() != null) statement.getUpdate().accept(this);
            this.insideLoopUpdate = false;

            if (statement.getBody() != null) statement.getBody().accept(this);
            this.insideLoop = false;

            super.visitForStatement(statement);
        }

        @Override
        public void visitIfStatement(PsiIfStatement statement) {
            statement.getCondition().accept(this);
            statement.getThenBranch().accept(this);
            if (statement.getElseBranch() != null) statement.getElseBranch().accept(this);

            super.visitIfStatement(statement);
        }

        @Override
        public void visitAssignmentExpression(PsiAssignmentExpression expression) {
            this.insideAssignmentLO = true;
            expression.getLExpression().accept(this);
            this.insideAssignmentLO = false;

            this.insideAssignmentRO = true;
            expression.getRExpression().accept(this);
            this.insideAssignmentRO = false;
            super.visitAssignmentExpression(expression);
        }

        @Override
        public void visitMethodCallExpression(PsiMethodCallExpression expression) {
            if (this.phase < 3 && this.insideLoop) {
                String varName = getVarFromExpression(expression);
                this.addMethodCall(expression);

                if (!this.insideLoopCondition) {
                    if ((!this.insideAssignmentRO && !this.insideAssignmentLO) || (this.insideAssignmentLO)) {
                        // Means this is a method call with potential state change.
                        // In other words, the variable used in this call cannot go outside the loop.
                        this.addConditionedVar(varName);
                    }
                }
            }
            super.visitMethodCallExpression(expression);
        }

        @Override
        public void visitReferenceExpression(PsiReferenceExpression expression) {
            if (this.insideAssignmentLO || this.insideLoopUpdate) {
                String varName = getVarFromExpression(expression);
                this.addConditionedVar(varName);
            }
            super.visitReferenceExpression(expression);
        }

        @Override
        public void visitWhileStatement(PsiWhileStatement statement) {
            this.insideLoop = true;
            statement.getCondition().accept(this);
            statement.getBody().accept(this);
            this.insideLoop = false;

            super.visitWhileStatement(statement);
        }

        @Override
        public void visitDoWhileStatement(PsiDoWhileStatement statement) {
            this.insideLoop = true;
            statement.getCondition().accept(this);
            statement.getBody().accept(this);
            this.insideLoop = false;

            super.visitDoWhileStatement(statement);
        }

        @Override
        public void visitMethod(PsiMethod method) {
            String fileName = StringUtils.getShortFileName(this.mContext.getJavaFile());
            String packageName = this.mContext.getJavaFile().getPackageName();
            visitingMethod = packageName + "." + fileName + "." + method.getName();
            super.visitMethod(method);
        }

        /* Visit methods for enclosing AST types */
        /* (only called when inside loop body) */
        @Override
        public void visitParenthesizedExpression(PsiParenthesizedExpression expression) {
            expression.getExpression().accept(this);
            super.visitParenthesizedExpression(expression);
        }

        @Override
        public void visitPostfixExpression(PsiPostfixExpression expression) {
            expression.getOperand().accept(this);
            super.visitPostfixExpression(expression);
        }

        @Override
        public void visitBinaryExpression(PsiBinaryExpression expression){
            expression.getLOperand().accept(this);
            expression.getROperand().accept(this);
        }

        @Override
        public void visitExpressionStatement(PsiExpressionStatement statement) {
            statement.getExpression().accept(this);
            super.visitExpressionStatement(statement);
        }

        @Override
        public void visitBlockStatement(PsiBlockStatement statement) {
            for (PsiStatement s : statement.getCodeBlock().getStatements()) {
                s.accept(this);
            }
            super.visitBlockStatement(statement);
        }

        @Override
        public void visitLocalVariable(PsiLocalVariable variable) {
            if (variable.getInitializer() != null) {
                this.addConditionedVar(variable.getName());
            }
            super.visitLocalVariable(variable);
        }

        @Override
        public void visitImplicitVariable(ImplicitVariable variable) {
            this.addConditionedVar(variable.getName());
            super.visitImplicitVariable(variable);
        }

        @Override
        public void visitDeclarationStatement(PsiDeclarationStatement statement) {
            for (PsiElement e : statement.getDeclaredElements()) {
                e.accept(this);
            }
            super.visitDeclarationStatement(statement);
        }
    }

    private class ClassVarsChecker extends JavaElementVisitor {

        private final JavaContext mContext;
        private int phase;

        public ClassVarsChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
        }

        @Override
        public void visitField(PsiField field) {
            classVars.add(field.getName());

            super.visitVariable(field);
        }
    }

    private class MethodCallChecker extends JavaElementVisitor {

        private final JavaContext mContext;
        private int phase;
        private List<String> methodArgs;

        private List<String> debugFiles = new ArrayList<>();
        private boolean lock;

        public MethodCallChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
            this.debugFiles.add("Debug.java");

            this.methodArgs = new ArrayList<>();
            this.lock = false;
        }

        private boolean argsConditioned() {
            for (String arg : methodArgs) {
                if (arg.equals("")) return true; // means we don't know nothing about this var,
                                                 // so we assume it is conditioned.
                if (conditionedVars.get(visitingMethod).contains(arg)) return true;
            }

            return false;
        }

        @Override
        public void visitMethodCallExpression(PsiMethodCallExpression expression) {
            if (this.phase == 3) {
                if (!this.lock) {
                    // this avoids analyzing arguments of method calls that are also method calls
                    this.lock = true;
                    String name = getVarFromExpression(expression);
                    // all method calls inside loops, belonging to the method `visitingMethod`.
                    List<String> calls = methodCalls.get(visitingMethod);
                    if (calls != null && calls.contains(expressionTag(mContext, expression))) {
                        // the method call being examined exists in the inside loop `calls`.
                        // if there call variable is not conditioned, it can be passed out the loop (issue found).
                        Set<String> vars = conditionedVars.get(visitingMethod);
                        // check if the arguments are conditioned or note
                        for (PsiExpression e : expression.getArgumentList().getExpressions()) {
                            e.accept(this);
                        }

                        if ((vars != null) && (!vars.contains(name)) && (!argsConditioned())) {
                            Reporter.reportIssue(mContext, ISSUE, expression);
                        }
                    }
                    lock = false;
                } else {
                    this.methodArgs.add(getVarFromExpression(expression));
                }
            }
            methodArgs.clear();

            super.visitMethodCallExpression(expression);
        }

        @Override
        public void visitMethod(PsiMethod method) {
            String fileName = StringUtils.getShortFileName(this.mContext.getJavaFile());
            String packageName = this.mContext.getJavaFile().getPackageName();
            visitingMethod = packageName + "." + fileName + "." + method.getName();
            super.visitMethod(method);
        }

        @Override
        public void visitReferenceExpression(PsiReferenceExpression expression) {
            methodArgs.add(getVarFromExpression(expression));
            super.visitReferenceExpression(expression);
        }

        @Override
        public void visitBinaryExpression (PsiBinaryExpression expression){
            methodArgs.add(getVarFromExpression(expression));
        }
        @Override
        public void visitArrayAccessExpression (PsiArrayAccessExpression expression){
            methodArgs.add(getVarFromExpression(expression));
        }

    }
}
