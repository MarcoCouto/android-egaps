package greenlab.org.ebugslocator.detectors;

import com.android.annotations.NonNull;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Location;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.android.tools.lint.detector.api.TextFormat;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiLocalVariable;
import com.intellij.psi.PsiVariable;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import greenlab.org.ebugslocator.utils.Reporter;

/**
 * Created by marco on 01-03-2018.
 */

public class HashMapUsageDetector extends Detector implements Detector.JavaPsiScanner {
    private static final Class<? extends Detector> DETECTOR_CLASS = HashMapUsageDetector.class;
    private static final EnumSet<Scope> DETECTOR_SCOPE = Scope.JAVA_FILE_SCOPE;

    private static final Implementation IMPLEMENTATION = new Implementation(
            DETECTOR_CLASS,
            DETECTOR_SCOPE
    );

    private static final String ISSUE_ID = "HashMapUsage";
    private static final String ISSUE_DESCRIPTION = "Use ArrayMap instead of HashMaps";
    private static final String ISSUE_EXPLANATION =
                "Using a `HashMap` data structure is discouraged. Most times they consume excessive " +
                "amounts of memory, a scarce resource os smartphones, which leads to an increase in " +
                "execution time and energy consumption.\n" +
                "\n" +
                "The use of a more lightweight alternative is advised: `ArrayMap`."
            ;
    private static final Category ISSUE_CATEGORY = Category.PERFORMANCE;
    private static final int ISSUE_PRIORITY = 5;
    private static final Severity ISSUE_SEVERITY = Severity.WARNING;

    public static final Issue ISSUE = Issue.create(
            ISSUE_ID,
            ISSUE_DESCRIPTION,
            ISSUE_EXPLANATION,
            ISSUE_CATEGORY,
            ISSUE_PRIORITY,
            ISSUE_SEVERITY,
            IMPLEMENTATION
    );

    public static final String mMapClass = "java.util.Map";
    public static final String mHashMapClass = "java.util.HashMap";

    public HashMapUsageDetector() {
    }

    /**
     * Selects the AST nodes for the detector to reach.
     * @return
     */
    @Override
    public List<Class<? extends PsiElement>> getApplicablePsiTypes() {
        return Arrays.<Class<? extends PsiElement>>asList(PsiLocalVariable.class, PsiVariable.class, PsiField.class);
    }

    @Override
    public void afterCheckProject(Context context) {
        super.afterCheckProject(context);
    }

    /**
     * Creates a Parse Tree visitor to analyze the AST of a file.
     * @param context
     * @return
     */
    @Override
    public JavaElementVisitor createPsiVisitor(@NonNull JavaContext context) {
        return new DeclarationChecker(context);
    }

    private static class DeclarationChecker extends JavaElementVisitor{

        private final JavaContext mContext;

        public DeclarationChecker(JavaContext context) {
            this.mContext = context;
        }

        private void checkAndReport(PsiVariable variable) {
            if (variable.getType() == null) return;
            String variableType = variable.getType().getCanonicalText();
            PsiExpression initializer = variable.getInitializer();

            if (variableType.startsWith(mHashMapClass)) {
                Reporter.reportIssue(mContext, ISSUE, variable);
            } else if (variableType.startsWith(mMapClass) && initializer != null && initializer.getType() != null) {
                String initializerType = initializer.getType().getCanonicalText();
                if (initializerType.startsWith(mHashMapClass)) {
                    Reporter.reportIssue(mContext, ISSUE, variable);
                }
            }
        }

        @Override
        public void visitVariable(PsiVariable variable) {
            checkAndReport(variable);
            super.visitVariable(variable);
        }

    }
}
