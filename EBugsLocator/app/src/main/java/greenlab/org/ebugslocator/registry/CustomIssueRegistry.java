package greenlab.org.ebugslocator.registry;

import com.android.tools.lint.client.api.IssueRegistry;
import com.android.tools.lint.detector.api.Issue;

import java.util.Arrays;
import java.util.List;

import greenlab.org.ebugslocator.detectors.CameraLeakDetector;
import greenlab.org.ebugslocator.detectors.ExcessiveCallsDetector;
import greenlab.org.ebugslocator.detectors.HashMapUsageDetector;
import greenlab.org.ebugslocator.detectors.InternalGetterDetector;
import greenlab.org.ebugslocator.detectors.MediaLeakDetector;
import greenlab.org.ebugslocator.detectors.MemberIgnoringMethodDetector;
import greenlab.org.ebugslocator.detectors.MemoizationChanceDetector;
import greenlab.org.ebugslocator.detectors.SensorLeakDetector;

/**
 * Created by marco on 28-02-2018.
 */

public class CustomIssueRegistry extends IssueRegistry {
    private List<Issue> mIssues = Arrays.asList(
            InternalGetterDetector.ISSUE,
            HashMapUsageDetector.ISSUE,
            ExcessiveCallsDetector.ISSUE,
            SensorLeakDetector.ISSUE,
            MediaLeakDetector.ISSUE,
            CameraLeakDetector.ISSUE,
            MemoizationChanceDetector.ISSUE,
            MemberIgnoringMethodDetector.ISSUE
    );

    public CustomIssueRegistry() {
    }

    @Override
    public List<Issue> getIssues() {
        return mIssues;
    }
}
