package greenlab.org.ebugslocator.checkers;

import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiVariable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import greenlab.org.ebugslocator.utils.Debug;
import greenlab.org.ebugslocator.utils.Reporter;

/**
 * Created by marco on 14-03-2018.
 */

public abstract class ResourceChecker extends JavaElementVisitor {

    private static final String mActivityClass = "android.app.Activity";
    private static final String mServiceClass = "android.app.Service";
    private static final List<String> mActivityExtends = Arrays.asList("android.accounts.AccountAuthenticatorActivity",
                                                                            "android.app.ActivityGroup",
                                                                            "android.app.AliasActivity",
                                                                            "android.app.ExpandableListActivity",
                                                                            "android.app.ListActivity",
                                                                            "android.app.LauncherActivity",
                                                                            "android.preference.PreferenceActivity",
                                                                            "android.app.NativeActivity",
                                                                            "android.app.FragmentActivity",
                                                                            "android.support.v4.app.FragmentActivity",
                                                                            "android.support.v7.app.AppCompatActivity",
                                                                            "android.app.TabActivity",
                                                                            "android.app.Fragment");

    private static final List<String> mServiceExtends = Arrays.asList("android.inputmethodservice.AbstractInputMethodService",
                                                                        "android.accessibilityservice.AccessibilityService",
                                                                        "android.service.autofill.AutofillService",
                                                                        "android.telecom.CallScreeningService",
                                                                        "android.service.media.CameraPrewarmService",
                                                                        "android.service.carrier.CarrierMessagingService",
                                                                        "android.service.carrier.CarrierService",
                                                                        "android.service.chooser.ChooserTargetService",
                                                                        "android.service.notification.ConditionProviderService",
                                                                        "android.telecom.ConnectionService",
                                                                        "android.app.admin.DeviceAdminService",
                                                                        "android.service.dreams.DreamService",
                                                                        "android.nfc.cardemulation.HostApduService",
                                                                        "android.nfc.cardemulation.HostNfcFService",
                                                                        "android.telecom.InCallService",
                                                                        "android.app.IntentService",
                                                                        "android.app.job.JobService",
                                                                        "android.service.media.MediaBrowserService",
                                                                        "android.media.midi.MidiDeviceService",
                                                                        "android.service.notification.NotificationListenerService",
                                                                        "android.nfc.cardemulation.OffHostApduService",
                                                                        "android.printservice.PrintService",
                                                                        "android.speech.RecognitionService",
                                                                        "android.widget.RemoteViewsService",
                                                                        "android.location.SettingInjectorService",
                                                                        "android.service.textservice.SpellCheckerService",
                                                                        "android.speech.tts.TextToSpeechService",
                                                                        "android.service.quicksettings.TileService",
                                                                        "android.media.tv.TvInputService",
                                                                        "android.telephony.VisualVoicemailService",
                                                                        "android.service.voice.VoiceInteractionService",
                                                                        "android.service.voice.VoiceInteractionSessionService",
                                                                        "android.net.VpnService",
                                                                        "android.service.vr.VrListenerService",
                                                                        "android.service.wallpaper.WallpaperService",
                                                                        "android.inputmethodservice.InputMethodService");

    private static Map<String, Set<String>> callStack = new HashMap<>();
    private static List<String> resourceVars = new ArrayList<>();
    private static ResourceUsage usageDetails = new ResourceUsage();

    private final JavaContext mContext;
    private Issue ISSUE;
    private int phase;
    private String resourceType;
    private String acquireMethod;
    private String releaseMethod;

    public ResourceChecker(JavaContext context,
                           int phase,
                           Issue issue,
                           String resourceType,
                           String acquireMethod,
                           String releaseMethod) {
        this.mContext = context;
        this.phase = phase;
        this.ISSUE = issue;
        this.resourceType = resourceType;
        this.acquireMethod = acquireMethod;
        this.releaseMethod = releaseMethod;
    }

    private void updateCallStack(String className, String methodName, String callName){
        String fullCallName = className + "." + callName;
        if (this.callStack.keySet().contains(methodName)) {
            this.callStack.get(methodName).add(fullCallName);
        }else{
            Set<String> calls = new HashSet<>();
            calls.add(fullCallName);
            this.callStack.put(methodName, calls);
        }
    }

    private boolean isResourceVar(String varName) {
        for (String var : this.resourceVars) {
            if (var.equals(varName)) {
                return true;
            }
        }
        return false;
    }

    private String getFullyQualifiedName(String visitingClass, String visitingMethod, String varName) {
        if (varName.equals("")) {
            return visitingClass + "." + visitingMethod;
        } else if (visitingMethod.equals("")) {
            return visitingClass + "." + varName;
        } else {
            return visitingClass + "." + visitingMethod + "." + varName;
        }
    }

    private boolean isCalleeReachable(Set<String> calledMethods, String callee, Set<String> visited) {
        if (calledMethods == null) return false;

        for (String m : calledMethods) {
            if (!visited.contains(m)) {
                visited.add(m);
                if (m.matches(callee)) {
                    return true;
                } else {
                    boolean check = isCalleeReachable(callStack.get(m), callee, visited);
                    if (check) return true;
                }
            }
        }
        return false;
    }

    private String getParentMethodName(PsiElement node){
        PsiElement parent = node;
        while (!(parent instanceof PsiMethod) && (parent != null)) {
            parent = parent.getParent();
        }
        if (parent == null) return "";
        else return ((PsiMethod)parent).getName();
    }

    private String getParentClassName(PsiElement node){
        PsiElement parent = node;
        while (!(parent instanceof PsiClass) && (parent != null)) {
            parent = parent.getParent();
        }
        if (parent == null) return "";
        else return ((PsiClass)parent).getQualifiedName();
    }

    private boolean flowCheck() {
        if (usageDetails.getReleaseClass().equals(usageDetails.getAcquireClass())) return true;

        String releaseMethod = usageDetails.getReleaseMethod();
        String acquireMethod = usageDetails.getAcquireMethod();

        for (String caller : callStack.keySet()) {
            Set<String> calledMethods = callStack.get(caller);
            if (calledMethods.contains(acquireMethod)) {
                return isCalleeReachable(calledMethods, releaseMethod, new HashSet<>());
            }
        }

        return false;
    }

    private void checkAndReport(PsiMethodCallExpression expression) {
        String visitingClass = getParentClassName(expression);
        String qualifiedName = expression.getMethodExpression()
                                         .getElement().getText()
                                         .replace("this.", "");
        String methodName = expression.getMethodExpression().getReferenceName();
        String varName = getFullyQualifiedName(visitingClass, "", qualifiedName.split("\\.")[0]);

        boolean isResourceMethod = methodName.matches(this.releaseMethod) || methodName.matches(this.acquireMethod);

        if (usageDetails.getVarName().equals(varName) && isResourceMethod) {
            if (usageDetails.getReleaseMethod().equals("") && !usageDetails.getAcquireMethod().equals("")) {
                // report... resource was not released
                Reporter.reportIssue(mContext, ISSUE, expression);
            } else if (usageDetails.isActivity) {
                boolean checkRelease = usageDetails.releaseMethodEndsWith("onPause")
                                    || usageDetails.releaseMethodEndsWith("onStop");
                if (methodName.matches(this.releaseMethod) && !checkRelease) {
                    Set<String> methodsFromOnStop = callStack.get(usageDetails.getAcquireClass()+".onStop");
                    Set<String> methodsFromOnPause = callStack.get(usageDetails.getAcquireClass()+".onPause");
                    boolean check = isCalleeReachable(methodsFromOnStop, usageDetails.getReleaseMethod(), new HashSet<>())
                                 || isCalleeReachable(methodsFromOnPause, usageDetails.getReleaseMethod(), new HashSet<>());

                    if (!check) {
                        // report... onPause can't reach the release method
                        Reporter.reportIssue(mContext, ISSUE, expression);
                    }
                } else {
                    // everything is ok!
                }
            } else if (usageDetails.isService) {
                // TODO: improve the analysis for services
                // for now, normal check
                if(!flowCheck()) {
                    //report... unreachable release method (Service)
                    Reporter.reportIssue(mContext, ISSUE, expression);
                }
            } else {
                // normal check
                if(!flowCheck()) {
                    //report... unreachable release method
                    Reporter.reportIssue(mContext, ISSUE, expression);
                }
            }
        }
    }

    public static void clear() {
        callStack.clear();
        usageDetails.clear();
    }

    @Override
    public void visitClass(PsiClass element) {
        if (this.phase == 1) {
            for (PsiClassType extendedClass : element.getExtendsListTypes()) {
                if (!usageDetails.isActivity) {
                    usageDetails.isActivity = mActivityExtends.contains(extendedClass.getCanonicalText());
                } else if (!usageDetails.isService) {
                    usageDetails.isService = mServiceExtends.contains(extendedClass.getCanonicalText());
                }
            }
        }
        super.visitClass(element);
    }

    @Override
    public void visitMethodCallExpression(PsiMethodCallExpression expression) {
        if (this.phase == 2) {
            String visitingClass = getParentClassName(expression);
            String visitingMethod = getParentMethodName(expression);

            String qualifiedName = expression.getMethodExpression()
                                             .getElement().getText()
                                             .replace("this.", "");
            String varName = getFullyQualifiedName(visitingClass, "", qualifiedName.split("\\.")[0]);
            String methodName = expression.getMethodExpression().getReferenceName();

            updateCallStack(visitingClass, visitingClass + "." + visitingMethod, qualifiedName);

            if (isResourceVar(varName)) {
                if (methodName.matches(this.acquireMethod)) {
                    usageDetails.setAcquireMethod(getFullyQualifiedName(visitingClass, visitingMethod, ""));
                    usageDetails.setAcquireClass(visitingClass);
                } else if (methodName.matches(this.releaseMethod)) {
                    usageDetails.setReleaseClass(visitingClass);
                    usageDetails.setReleaseMethod(getFullyQualifiedName(visitingClass, visitingMethod, ""));
                }
            }
        } else if (phase == 3) {
            checkAndReport(expression);
        }
        super.visitMethodCallExpression(expression);
    }

    @Override
    public void visitVariable(PsiVariable variable) {
        if (this.phase == 1) {
            PsiType varType = variable.getType();
            if (varType != null && varType.getCanonicalText().equals(this.resourceType)) {
                String visitingClass = getParentClassName(variable);
                String visitingMethod = getParentMethodName(variable);

                String qualifiedName = getFullyQualifiedName(visitingClass, visitingMethod, variable.getName());
                this.resourceVars.add(qualifiedName);
                usageDetails.setVarName(qualifiedName);
            }
        }
        super.visitVariable(variable);
    }

    private static class ResourceUsage {

        private String varName;
        private String acquireClass;
        private String acquireMethod;
        private String releaseClass;
        private String releaseMethod;

        public boolean isActivity;
        public boolean isService;

        public ResourceUsage() {
            this.varName = "";
            this.acquireClass = "";
            this.acquireMethod = "";
            this.releaseClass = "";
            this.releaseMethod = "";


            this.isActivity = false;
            this.isService = false;
        }

        public ResourceUsage(String varName,
                             String acquireClass,
                             String acquireMethod,
                             String releaseClass,
                             String releaseMethod) {
            this.varName = varName;
            this.acquireClass = acquireClass;
            this.acquireMethod = acquireMethod;
            this.releaseClass = releaseClass;
            this.releaseMethod = releaseMethod;

            this.isActivity = false;
            this.isService = false;
        }

        public String getVarName() {
            return varName;
        }

        public void setVarName(String varName) {
            this.varName = varName;
        }

        public String getAcquireMethod() {
            return acquireMethod;
        }

        public void setAcquireMethod(String acquireMethod) {
            this.acquireMethod = acquireMethod;
        }

        public String getReleaseMethod() {
            return releaseMethod;
        }

        public boolean releaseMethodEndsWith(String suffix) {
            String[] split = this.releaseMethod.split("\\|");
            for (String s : split) {
                if (s.endsWith(suffix)) return true;
            }
            return false;
        }

        public String getAcquireClass() {
            return acquireClass;
        }

        public void setAcquireClass(String acquireClass) {
            this.acquireClass = acquireClass;
        }

        public String getReleaseClass() {
            return releaseClass;
        }

        public void setReleaseClass(String releaseClass) {
            this.releaseClass = releaseClass;
        }

        public void setReleaseMethod(String releaseMethod) {
            if (this.releaseMethod.equals("")) this.releaseMethod = releaseMethod;
            else this.releaseMethod += "|" + releaseMethod;
        }

        public void clear() {
            this.acquireMethod = "";
            this.releaseMethod = "";

            this.isActivity = false;
            this.isService = false;
        }

        public String toString() {
            String res = "[Activity: " + isActivity + "]\n[Service: " + isService + "]\n";
            res += "\tvarName => " + varName + "\n";
            res += "\tAcq Class => " + acquireClass + "\n";
            res += "\tRel Class => " + releaseClass + "\n";
            res += "\tAcq Method => " + acquireMethod + "\n";
            res += "\tRel Method => " + releaseMethod + "\n";

            return res;
        }

    }

}
