package greenlab.org.ebugslocator.detectors;

import com.android.annotations.NonNull;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiAnonymousClass;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiModifierList;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiThisExpression;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import greenlab.org.ebugslocator.utils.Debug;
import greenlab.org.ebugslocator.utils.Reporter;

/**
 * Created by marco on 01-03-2018.
 */

public class MemberIgnoringMethodDetector extends Detector implements Detector.JavaPsiScanner {
    private static final Class<? extends Detector> DETECTOR_CLASS = MemberIgnoringMethodDetector.class;
    private static final EnumSet<Scope> DETECTOR_SCOPE = Scope.JAVA_FILE_SCOPE;

    private static final Implementation IMPLEMENTATION = new Implementation(
            DETECTOR_CLASS,
            DETECTOR_SCOPE
    );

    private static final String ISSUE_ID = "MemberIgnoringMethod";
    private static final String ISSUE_DESCRIPTION = "non-static method that can be made static";
    private static final String ISSUE_EXPLANATION =
                "Methods that do not have a static modifier, yet never access a field or a non-static "+
                "method, can be converted to static."
            ;
    private static final Category ISSUE_CATEGORY = Category.PERFORMANCE;
    private static final int ISSUE_PRIORITY = 5;
    private static final Severity ISSUE_SEVERITY = Severity.WARNING;

    public static final Issue ISSUE = Issue.create(
            ISSUE_ID,
            ISSUE_DESCRIPTION,
            ISSUE_EXPLANATION,
            ISSUE_CATEGORY,
            ISSUE_PRIORITY,
            ISSUE_SEVERITY,
            IMPLEMENTATION
    );

    private static Map<String, Boolean> fields;
    private static Map<String, Set<String>> methods;
    private static Map<String, Boolean> methodsStatus;

    public MemberIgnoringMethodDetector() {
        fields = new HashMap<>();
        methods = new HashMap<>();
        methodsStatus = new HashMap<>();
    }

    public static String getMethodQualifiedName(PsiMethod method) {
        PsiClass parent = method.getContainingClass();
        if (parent != null) {
            return parent.getQualifiedName() + "." + method.getName();
        }

        return null;
    }

    private static void isMIM(String methodName) {
        methodsStatus.put(methodName, true);
    }

    private static void notMIM(String methodName) {
        methodsStatus.put(methodName, false);
    }

    private static boolean isField(String qualifiedName) {
        if (fields.containsKey(qualifiedName)) return fields.get(qualifiedName);

        return false;
    }

    private void classifyMIM(String methodName) {
        if (methodsStatus.containsKey(methodName)) {
            return;
        }
        if (! methods.containsKey(methodName)){
            // it's a method that we have no information about. Hence, it can either be a MIM or not.
            // since we MUST avoid having false positives, in this cases we assume is a non-MIM.
            notMIM(methodName);
            return;
        }
        for (String depend : methods.get(methodName)) {
            /*if (depend.matches(".+\\.[A-Z]\\w+$")) {
                // if the dependency is a class, than it's not a dependency.
                // TODO: the if condition must be reviewed.
            }
            */

            if (isField(depend)) {
                notMIM(methodName); return;
            } else {
                classifyMIM(depend);
                if (methodsStatus.containsKey(depend) && !methodsStatus.get(depend)) {
                    notMIM(methodName);
                    return;
                }
            }
        }
        isMIM(methodName);
    }

    private void checkDependencies() {
        for (String method : methods.keySet()) {
            classifyMIM(method);
        }
    }

    /**
     * Selects the AST nodes for the detector to reach.
     * @return
     */
    @Override
    public List<Class<? extends PsiElement>> getApplicablePsiTypes() {
        return Arrays.<Class<? extends PsiElement>>asList(
                PsiField.class,
                PsiReferenceExpression.class,
                PsiThisExpression.class,
                PsiMethod.class);
    }

    @Override
    public void afterCheckProject(Context context) {
        if (context.getDriver().getPhase() < 2) {
            checkDependencies();
            //debug();
            context.getDriver().requestRepeat(this, DETECTOR_SCOPE);
        }
        super.afterCheckProject(context);
    }

    public void debug() {
        System.out.println("\t << FIELDS >>");
        for (String f : fields.keySet()) {
            System.out.println("\t\t>> " + f + " [" + fields.get(f) + "]");
        }

        System.out.println("\t ** METHODS **");
        for (String m : methods.keySet()) {
            System.out.print("\t\t** " + m + " => [ ");
            for (String d :  methods.get(m)) {
                System.out.print(d + ", ");
            }
            System.out.println(" ]");
        }

        System.out.println("\t ## STATUS ##");
        for (String s : methodsStatus.keySet()) {
            System.out.println("\t\t## " + s + " [" + methodsStatus.get(s) + "]");
        }
    }

    /**
     * Creates a Parse Tree visitor to analyze the AST of a file.
     * @param context
     * @return
     */
    @Override
    public JavaElementVisitor createPsiVisitor(@NonNull JavaContext context) {
        int phase = context.getDriver().getPhase();
        if (phase == 1){
            return new MemberIgnoringChecker(context);
        } else {
            return new MethodChecker(context);
        }

    }

    private static class MemberIgnoringChecker extends JavaElementVisitor{

        private final JavaContext mContext;

        public MemberIgnoringChecker(JavaContext context) {
            this.mContext = context;
        }

        private void addField(PsiField field, PsiClass containingClass, boolean isNonStatic) {
            String fullName = containingClass.getQualifiedName() + "." + field.getName();
            fields.put(fullName, isNonStatic);
        }

        private void addDependency(PsiMethod parentMethod, String reference) {
            String methodName = getMethodQualifiedName(parentMethod);
            if (!methodName.equals(reference)) {
                if (methods.containsKey(methodName)) {
                    methods.get(methodName).add(reference);
                } else {
                    Set<String> refs = new HashSet<>();
                    refs.add(reference);
                    methods.put(methodName, refs);
                }
            }
        }

        private PsiMethod getParentMethod(PsiElement node) {
            if (node == null) return null;

            PsiElement parent = node.getParent();
            if (parent == null) return null;

            if (parent instanceof PsiMethod) {
                return (PsiMethod) parent;
            } else {
                return getParentMethod(parent);
            }
        }

        @Override
        public void visitField(PsiField field) {
            PsiModifierList modifiers = field.getModifierList();
            boolean isNonStatic = modifiers != null && !modifiers.hasModifierProperty("static");
            addField(field, field.getContainingClass(), isNonStatic);

            super.visitField(field);
        }

        @Override
        public void visitReferenceExpression(PsiReferenceExpression expression) {
            PsiMethod parent = getParentMethod(expression);
            if (parent != null) {
                PsiElement elem = expression.getOriginalElement();
                if (elem != null) {
                    String qualifiedName = expression.getQualifiedName();
                    String originalText = elem.getText().replace("this.", "");
                    if ((!originalText.contains("."))             // i.e., is not a method invoked from an object
                            && qualifiedName.contains(".")        // i.e., is not a local variable
                            ) {
                        addDependency(parent, qualifiedName);
                    }
                } else {
                    // If we reach this point, it means that the method where this
                    // 'ReferenceExpression' was found could not be properly evaluated.
                    // As such, we assume that such method is not a MIM.
                    notMIM(getMethodQualifiedName(parent));
                }
            }
            super.visitReferenceExpression(expression);
        }

        @Override
        public void visitMethod(PsiMethod method) {
            String qualifiedName = getMethodQualifiedName(method);

            PsiModifierList modifiers = method.getModifierList();
            PsiAnnotation override = modifiers.findAnnotation("java.lang.Override");
            boolean isInterface = method.getContainingClass() != null && method.getContainingClass().isInterface();

            boolean emptyBody = method.getBody() == null
                    || method.getBody().getStatements() == null
                    || method.getBody().getStatements().length == 0;

            boolean ignorable = method.isConstructor()
                    || isInterface
                    || (method.getContainingClass() instanceof PsiAnonymousClass);

            if (ignorable) return;

            if (! methods.containsKey(qualifiedName)) {
                methods.put(qualifiedName, new HashSet<>());    //
            }

            boolean cannotBeMIM = override != null
                    || emptyBody
                    || method.getContainingClass().getModifierList().hasModifierProperty("abstract");

            PsiMethod[] supers = method.findSuperMethods();

            if (modifiers.hasModifierProperty("static")) {
                isMIM(qualifiedName);
            } else if (cannotBeMIM) {
                notMIM(qualifiedName);
            } else if (supers != null && supers.length > 0) {
                notMIM(qualifiedName);
                for (PsiMethod s : supers) {
                    if (s != null) {
                        String superMethod = getMethodQualifiedName(s);
                        if (! methods.containsKey(superMethod)) methods.put(superMethod, new HashSet<>());
                        addDependency(method, superMethod);
                        notMIM(superMethod);
                    }
                }
            }
        }

        @Override
        public void visitThisExpression(PsiThisExpression expression) {
            PsiMethod parent = getParentMethod(expression);
            if (parent != null) {
                notMIM(getMethodQualifiedName(parent));
            }
            super.visitThisExpression(expression);
        }
    }

    private static class MethodChecker extends JavaElementVisitor{

        private final JavaContext mContext;

        public MethodChecker(JavaContext context) {
            this.mContext = context;
        }

        @Override
        public void visitMethod(PsiMethod method) {
            PsiModifierList modifiers = method.getModifierList();
            PsiAnnotation override = modifiers.findAnnotation("java.lang.Override");
            boolean isInterface = method.getContainingClass() != null && method.getContainingClass().isInterface();
            boolean emptyBody = method.getBody() == null
                    || method.getBody().getStatements() == null
                    || method.getBody().getStatements().length == 0;

            boolean ignorable = method.isConstructor()
                    || modifiers.hasModifierProperty("static")
                    || override != null
                    || isInterface
                    || (method.getContainingClass() instanceof PsiAnonymousClass)
                    || emptyBody;

            if (ignorable) {
                super.visitMethod(method);
                return;
            }

            PsiMethod[] supers = method.findSuperMethods();
            for (PsiMethod s : supers) {
                if (s != null) {
                    ignorable = true;  // if there is a super method, it means this method is
                    // already respecting the super's signature. turning
                    // this one into static would be impossible.
                    break;
                }
            }

            if (ignorable) {
                super.visitMethod(method);
                return;
            }

            String methodName = getMethodQualifiedName(method);
            if ((!methodsStatus.containsKey(methodName)) || methodsStatus.get(methodName)) {
                Reporter.reportIssue(mContext, ISSUE, method);
            }

            super.visitMethod(method);
        }
    }

}
