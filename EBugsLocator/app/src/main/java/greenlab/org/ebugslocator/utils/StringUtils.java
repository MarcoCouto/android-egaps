package greenlab.org.ebugslocator.utils;

import com.intellij.psi.PsiJavaFile;

/**
 * Created by marco on 24-03-2018.
 */

public class StringUtils {

    public static String getShortFileName(PsiJavaFile file) {
        String[] splitName = file.getName().split("/");
        String shortName = splitName[splitName.length -1].split("\\.")[0];
        return shortName;
    }
}
