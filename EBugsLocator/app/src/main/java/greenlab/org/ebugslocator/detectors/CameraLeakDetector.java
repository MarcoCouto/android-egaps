package greenlab.org.ebugslocator.detectors;

import com.android.annotations.NonNull;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiVariable;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import greenlab.org.ebugslocator.checkers.ResourceChecker;

/**
 * Created by marco on 13-03-2018.
 */

public class CameraLeakDetector extends Detector implements Detector.JavaPsiScanner {
    private static final Class<? extends Detector> DETECTOR_CLASS = CameraLeakDetector.class;
    private static final EnumSet<Scope> DETECTOR_SCOPE = Scope.JAVA_FILE_SCOPE;

    private static final Implementation IMPLEMENTATION = new Implementation(
            DETECTOR_CLASS,
            DETECTOR_SCOPE
    );

    private static final String ISSUE_ID = "CameraLeak";
    private static final String ISSUE_DESCRIPTION = "Keeping the camera active without using it";
    private static final String ISSUE_EXPLANATION = "Detailed explanation";  //TODO: improve explanation
    private static final Category ISSUE_CATEGORY = Category.PERFORMANCE;
    private static final int ISSUE_PRIORITY = 5;
    private static final Severity ISSUE_SEVERITY = Severity.WARNING;

    public static final Issue ISSUE = Issue.create(
            ISSUE_ID,
            ISSUE_DESCRIPTION,
            ISSUE_EXPLANATION,
            ISSUE_CATEGORY,
            ISSUE_PRIORITY,
            ISSUE_SEVERITY,
            IMPLEMENTATION
    );

    private static final String mSensorManagerClass = "android.hardware.Camera";
    private static final String mAcquireMethod = "Camera.open";
    private static final String mReleaseMethod = "release";


    public CameraLeakDetector() {
    }

    @Override
    public List<Class<? extends PsiElement>> getApplicablePsiTypes() {
        return Arrays.<Class<? extends PsiElement>>asList(
                PsiClass.class,
                PsiMethod.class,
                PsiMethodCallExpression.class,
                PsiVariable.class);
    }

    @Override
    public JavaElementVisitor createPsiVisitor(@NonNull JavaContext context) {
        return new CameraChecker(context,
                context.getDriver().getPhase(),
                this.ISSUE,
                mSensorManagerClass,
                mAcquireMethod,
                mReleaseMethod);
    }

    @Override
    public void beforeCheckProject(Context context) {
        if (context.getDriver().getPhase() == 1) {
            CameraChecker.clear();
        }
        super.beforeCheckProject(context);
    }

    @Override
    public void afterCheckProject(Context context) {
        if (context.getDriver().getPhase() == 1) {
            context.getDriver().requestRepeat(this, DETECTOR_SCOPE);
        } else if (context.getDriver().getPhase() == 2) {
            context.getDriver().requestRepeat(this, DETECTOR_SCOPE);
        }
        super.afterCheckProject(context);
    }

    private class CameraChecker extends ResourceChecker {
        public CameraChecker(JavaContext context,
                             int phase,
                             Issue issue,
                             String resourceType,
                             String acquireMethod,
                             String releaseMethod) {

            super(context, phase, issue, resourceType, acquireMethod, releaseMethod);
        }
    }


}
